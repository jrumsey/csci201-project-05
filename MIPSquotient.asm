
# num holds the numerator, denom holds the denominator;
# calculate quotient of num / denom and place in quotient

.data
num:      .word 77
denom:    .word 9
quotient: .word 0

.text
.globl main

main:

	lw $t0, num
	lw $t1, denom
	lw $t2, quotient
	div $t2, $t0, $t1

    jr $ra