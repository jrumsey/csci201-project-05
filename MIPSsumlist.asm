# the variable count contains the number of
# bytes in the vals array; the number of bytes
# will be 4 * number of elements; so the last
# valid memory position in the array is count-4

# store the sum of the array in the variable sum
        
.data
count:  .word 20                # 5 words * 4 bytes per word
vals:   .word 1, 2, 3, 4, 5
sum:    .word 0
        
.text   
.globl main

main:

	lw $t0, count    	
	la $t1, vals
	lw $t2, sum
	lw $t3, ($t1)    	
	lw $t4, 4($t1)   	
	lw $t5, 8($t1) 	
	lw $t6, 12($t1)
	lw $t7, 16($t1)
	lw $t8, 20($t1)
	add $t2, $t3, $t4
	add $t2, $t2, $t5
	add $t2, $t2, $t6
	add $t2, $t2, $t7
	add $t2, $t2, $t8
	

    jr $ra                   # terminate
        
