
# num holds the numerator, denom holds the denominator;
# calculate remainder of num % denom and place in remainder

.data
num:       .word 77
denom:     .word 9
remainder: .word 0

.text
.globl main

main:

    lw $t0, num
	lw $t1, denom
	lw $t2, remainder
	LoopA:
	sub $t0, $t0, $t1
	bge $t0, $0, LoopA
	add $t2, $t0, $t1

    jr $ra