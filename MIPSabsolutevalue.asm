
# val holds the original value, absolute value should
# be calculated and saved into absval

.data
val:    .word -10
absval: .word 0
negone:	.word -1

.text
.globl main

main:

	lw $t0, val
	lw $t1, absval
	lw $t2, negone
	bge $t0, $0, Done
	mul $t1, $t0, $t2 
	Done:
    jr $ra